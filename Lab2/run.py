with open('hosts.txt', 'r') as hosts:
    blacklist_hosts = [row.strip() for row in hosts]

with open('dns.log', 'r') as dns:
    list_my_hosts = dns.readlines()

entries = 0
count_hosts = 0
for my_host in list_my_hosts:
    count_hosts+=1
    data = my_host.split("\t")
    if len(data) > 2 and data[9] in blacklist_hosts:
        entries += 1

print()
print(f"{entries}/{count_hosts} - нежелательные хосты в дампе трафика.")
print(f"Нежелательный трафик составил: {entries/count_hosts*100}%")
print()
